require 'elasticsearch/model'
class User < ActiveRecord::Base
  include Elasticsearch::Model
  include Elasticsearch::Model::Callbacks
  before_save :create_permalink, on: :create
  #before_save :index_elasticsearch_document, on: :update
  rolify
  validates_presence_of :name, :role_ids

  #has_many :posts, dependent: :destroy relation posts
  
  # Include default devise modules. Others available are:
  # :confirmable, :lockable, :timeoutable and :omniauthable
  devise :database_authenticatable, :registerable,
         :recoverable, :rememberable, :trackable, :validatable

    def as_json(options = {})
      {
        id: self.id,
        role_name: role_name,
        rol: self.roles.first.id,
        email: self.email,
        name: self.name,
        permalink: self.permalink,
        created_at: self.created_at,
        sign_in_count: self.sign_in_count
      }
    end

  def role_name
    self.roles.first.name
  end
  
  def self.group_by_roles
    rtrn = {}
    [:admin, :moderator, :redactor].each do |rol|
      rtrn[rol] = User.with_role(rol).count
    end
    rtrn
  end

  def self.get_id(permalink)
    User.find_by_permalink(permalink).id
  end

  def self.query(query)
    { query: { multi_match:  { query: query, fields: [:role_name, :name, :email, :permalink] , operator: :and }  }, sort: { id: "desc" }, size: User.count }
  end

  #params[:search] is false
  def self.index(current_user)
    User.where("id != #{current_user.id}").order("created_at DESC")
  end

  #params[:search] is true
  def self.index_search(objects, current_user)
    objects.where("id != #{current_user.id}").order("created_at DESC")
  end

  #params[:search] is true
  def self.index_total(objects, current_user)
    objects.where("id != #{current_user.id}").count
  end

  def self.route_index
    "/admin/users"
  end

  def index_elasticsearch_document
    self.__elasticsearch__.index_document
  end

  def create_permalink
    self.permalink = self.name.downcase.parameterize+"-"+SecureRandom.hex(4)
  end

  def self.multiple_destroy(ids, current_user)
    ids.each do |id|
      raise CanCan::AccessDenied.new("Access Denied", :delete, User) if id.to_i == current_user.id
    end
    User.destroy ids
  end

end

User.import