class Ability
  include CanCan::Ability

  def initialize(user)
    user ||= User.new # guest user (not logged in)
    if user.has_role? :admin

      # - user authorize -
      can [:delete, :show, :edit, :update, :create, :index], User
      can :destroy, User do |u|
        u.id != user.id
      end
      # - end user authorize -

      # - blog authorize -
      can :manage, InyxBlogRails::Post if EngineRails.list_engines.include? :inyx_blog_rails
      can :manage, InyxBlogRails::Category if EngineRails.list_engines.include? :inyx_blog_rails
      # - end blog authorize -

       # - catalog authorize -
      can :manage, InyxCatalogRails::Catalog if EngineRails.list_engines.include? :inyx_catalog_rails
      can :manage, InyxCatalogRails::Attachment if EngineRails.list_engines.include? :inyx_catalog_rails
      # - end blog authorize -

      # - contact_us authorize -
      can :manage, InyxContactUsRails::Message if EngineRails.list_engines.include? :inyx_contact_us_rails
      # - end cntact_us authorize -

    elsif user.has_role? :moderator

      # - user authorize -
      can :read, User
      # - end user authorize -

      # - blog authorize -
      can [:read, :edit, :update], InyxBlogRails::Post if EngineRails.list_engines.include? :inyx_blog_rails
      can [:read, :new, :create], InyxBlogRails::Category if EngineRails.list_engines.include? :inyx_blog_rails
      # - end blog authorize -

      # - catalog authorize -
      can [:read], InyxCatalogRails::Catalog  if EngineRails.list_engines.include? :inyx_catalog_rails
      can [:read, :new, :create], InyxCatalogRails::Attachment  if EngineRails.list_engines.include? :inyx_catalog_rails
      # - end blog authorize -


      # - contact_us authorize -
      can :read, InyxContactUsRails::Message if EngineRails.list_engines.include? :inyx_contact_us_rails
      # - end contact_us authorize -

    elsif user.has_role? :redactor

      # - blog authorize -
      can :manage, InyxBlogRails::Post, :user_id => user.id if EngineRails.list_engines.include? :inyx_blog_rails
      # - end blog authorize -
      
    end
    # The first argument to `can` is the action you are giving the user 
    # permission to do.
    # If you pass :manage it will apply to every action. Other common actions
    # here are :read, :create, :update and :destroy.
    #
    # The second argument is the resource the user can perform the action on. 
    # If you pass :all it will apply to every resource. Otherwise pass a Ruby
    # class of the resource.
    #
    # The third argument is an optional hash of conditions to further filter the
    # objects.
    # For example, here the user can only update published articles.
    #
    #   can :update, Article, :published => true
    #
    # See the wiki for details:
    # https://github.com/ryanb/cancan/wiki/Defining-Abilities
  end
end
