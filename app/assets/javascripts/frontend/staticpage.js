function removeHash () { 
  history.pushState("", document.title, window.location.pathname + window.location.search);
}

$( document ).ready(function() {

	$('.top-nav').onePageNav({
    currentClass: 'current',
    changeHash: false,
    scrollSpeed: 500,
    filter: ':not(.external)',
    begin: function() {
      removeHash();
    },
    end: function() {
    	
    	if ($('.top-nav .current').find('a').attr('href') == "#about") {
        $('.mobile-show').animate({
          opacity: 1,
          left: "+=600"
        }, 300, function() {
          $("#mobile-img").removeClass("mobile-show")
        });
      }

      if($('.top-nav .current').find('a').attr('href') != "#home") {
        $("#front-navbar").removeClass("background-hide");
        $("#front-navbar").addClass("background-show");
      } else {
        $("#front-navbar").removeClass("background-show");
        $("#front-navbar").addClass("background-hide");
      }

    },
    scrollChange: function($currentListItem) {
     
      if($(".top-nav .current a").attr("href") == "#about"){
        $('.mobile-show').animate({
          opacity: 1,
          left: "+=600"
        }, 300, function() {
          $("#mobile-img").removeClass("mobile-show")
        });
      }      		  
    }
  });

  window.onscroll = function (event) {
    if($(window).scrollTop() <= 100) {
      $("#front-navbar").removeClass("background-show");
      $("#front-navbar").addClass("background-hide");
    } else {
      $("#front-navbar").removeClass("background-hide");
      $("#front-navbar").addClass("background-show");
    }
  }
});

