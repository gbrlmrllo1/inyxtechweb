class FrontendController < ApplicationController
	layout 'layouts/frontend/application'
  def index
  	Location.add(request.remote_ip)
    @attachments = InyxCatalogRails::Attachment.where(public: true).limit(6);
  end
end
