Rails.application.routes.draw do

  root to: 'frontend#index'

  get "services/software-development", to: "frontend#software_development", as: "software_development"
  get "services/web-design", to: "frontend#web_design", as: "web_design"

  mount InyxContactUsRails::Engine, :at => '', as: 'messages'
  mount InyxCatalogRails::Engine, :at => '', as: 'catalog'
  
  devise_for :users, skip: [:registration]

  resources :admin, only: [:index]

  scope :admin do
  	resources :users do
  		collection do
	  		post '/delete', to: 'users#delete'
  		end
  	end
  end
end
