# Agregar datos de configuración
InyxContactUsRails.setup do |config|
	config.mailer_to = "contact@inyxtech.com"
	config.mailer_from = "contact@inyxtech.com"
	config.name_web = "Inyxtech"
	# Activar o Desactivar la ruta especificada ("messages/new")
	config.messages_new = false
	#Route redirection after send
	config.redirection = "/#contact_us"


	# Agregar keys de google recaptcha
	Recaptcha.configure do |config|
	  config.public_key  = "6LdjRQITAAAAAJCtNiTT_AQxSZqSHaftEKRcAyad"
	  config.private_key = "6LdjRQITAAAAAJpQM6PT2cnplHOpOaiDw5RIUVQw"
	end
end